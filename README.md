# Poker Style Deck
This project contains a single module that contains classes to define a poker style deck.

The deck will initially contain 52 cards one for each combination of suite and face values. The deck can be shuffled and cards can be dealt one at a time, which will remove it from the deck.

## Requirements
* Python 3.4+

## Running Tests
Unit tests uses the standard `unittest` module and can be run using the command `python3 -m unittest discover` from the project's root directory
