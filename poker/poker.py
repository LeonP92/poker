"""
Poker Deck

This module provides classes that create a poker style deck where
a deck consists of cards that has a suite and face value
"""

import random
from enum import Enum


class Suite(Enum):
    HEARTS = 0
    CLUBS = 1
    SPADES = 2
    DIAMONDS = 3


class FaceValue(Enum):
    ACE = 1
    TWO = 2
    THREE = 3
    FOUR = 4
    FIVE = 5
    SIX = 6
    SEVEN = 7
    EIGHT = 8
    NINE = 9
    TEN = 10
    JACK = 11
    QUEEN = 12
    KING = 13


class Deck(object):
    """
    Class representing a deck of cards a deck when first created will have 52
    cards. A deck can be shuffled to randomly arrange the cards within it and
    cards can be dealt
    """

    def __init__(self):
        """
        Create a new deck of cards. The deck will be populated with 52 cards
        for each face value and suite.
        """
        self._cards = []

        for val in FaceValue:
            self._cards += [
                Card(suite=suite, value=val) for suite in Suite
            ]

    def shuffle(self):
        """
        Move each card in the deck around into a different location in the deck
        """

        for idx in range(0, len(self._cards) - 1):
            '''Find a random integer between 0 and i to determine the index
            to swap the card with. Not doing len(self._cards) - 1 to prevent
            any one card being the card to swap twice.
            '''
            new_idx = random.randint(0, idx)

            self._cards[idx], self._cards[new_idx] = self._cards[new_idx], self._cards[idx]

    def dealOneCard(self):
        """
        Returns the top card on the deck. If there are none left will return None
        """
        try:
            return self._cards.pop()
        except IndexError:
            return None


class Card(object):
    """
    Class representing a single card in a deck each. Cards have a face value
    and a suite.
    """

    def __init__(self, suite: Suite, value: FaceValue):
        self.suite = suite
        self.face_value = value

    def __str__(self):
        """
        Returns the string representation of a card in the format of
        <FACE_VALUE> of <SUITE>
        """
        return '%s of %s' % (self.face_value.name.title(), self.suite.name.title())

    def __eq__(self, other):
        """
        Compares two cards and returns True if the card's face value and suite
        matches else returns False
        """
        if isinstance(other, Card):
            return self.suite == other.suite and self.face_value == other.face_value

        return False
