import unittest

from poker.poker import Card, Suite, FaceValue


class TestCard(unittest.TestCase):

    def test_string_representation(self):
        """
        Tests to make sure the string representation of the card is
        <FACE_VALUE> of <SUITE> where the <FACE_VALUE> and <SUITE> is the
        title case names for the enums
        """
        card = Card(suite=Suite.HEARTS, value=FaceValue.ACE)
        self.assertEqual(str(card), 'Ace of Hearts')

    def test_equals(self):
        """
        Test to make sure that comparing two cards compares the suite
        and face value
        """
        first_card = Card(suite=Suite.HEARTS, value=FaceValue.ACE)
        second_card = Card(suite=Suite.HEARTS, value=FaceValue.ACE)
        self.assertTrue(first_card == second_card, True)

        second_card = Card(suite=Suite.SPADES, value=FaceValue.QUEEN)
        self.assertFalse(first_card == second_card, False)
