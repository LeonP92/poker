import copy
import unittest

from poker.poker import Deck, Card, Suite, FaceValue


class TestDeck(unittest.TestCase):

    def test_init(self):
        """
        Test to make sure that each new deck has 52 cards and contains all of
        the variations of cards (all suites for all face values)
        """
        deck = Deck()
        self.assertEqual(len(deck._cards), 52)

        for val in FaceValue:
            for suite in Suite:
                self.assertTrue(
                    Card(suite=suite, value=val) in deck._cards
                )

    def test_shuffle(self):
        """
        Tests the deck's shuffle method to ensure that the deck's card order
        has been changed
        """
        deck = Deck()
        original_deck = copy.deepcopy(deck._cards)
        deck.shuffle()
        self.assertNotEqual(deck._cards, original_deck)

        original_deck = copy.deepcopy(deck._cards)
        deck.shuffle()
        self.assertNotEqual(deck._cards, original_deck)

    def test_shuffle_no_duplicates(self):
        """
        Test to make sure that there are no duplicates in the deck after a
        a shuffle call
        """
        already_dealt_cards = []
        deck = Deck()
        deck.shuffle()

        for idx, card in enumerate(deck._cards):
            self.assertFalse(card in already_dealt_cards)
            already_dealt_cards.append(card)

    def test_deal_one(self):
        """
        Test deal one of a new deck to make sure that each card is unique
        """
        deck = Deck()
        already_dealt_cards = []

        for _ in range(0, 52):
            card = deck.dealOneCard()
            self.assertFalse(card in already_dealt_cards)
            already_dealt_cards.append(card)

    def test_exhaust_deck(self):
        """
        Knowing that there are 52 cards, call the dealOneCard 52 times and make
        sure that the next call will return None
        """
        deck = Deck()
        for _ in range(0, 52):
            self.assertIsNotNone(deck.dealOneCard())

        self.assertIsNone(deck.dealOneCard())


if __name__=='__main__':
    unittest.main()
